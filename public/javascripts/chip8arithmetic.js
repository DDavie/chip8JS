

export function arithmeticSet(myChip8){
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = myChip8.v[(myChip8.opcode & 0x00f0) >> 4];
	return 1;
}

export function arithmeticSetOr(myChip8){
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] =
		myChip8.v[(myChip8.opcode & 0x0f00) >> 8] |
		myChip8.v[(myChip8.opcode & 0x00f0) >> 4];
	return 1;
}

export function arithmeticSetAnd(myChip8){
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] =
		myChip8.v[(myChip8.opcode & 0x0f00) >> 8] &
		myChip8.v[(myChip8.opcode & 0x00f0) >> 4];
	return 1;
}

export function arithmeticSetXor(myChip8){
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] =
		myChip8.v[(myChip8.opcode & 0x0f00) >> 8] ^ 
		myChip8.v[(myChip8.opcode & 0x00f0) >> 4];
	return 1;
}

export function arithmeticAdd(myChip8){
	var vx = myChip8.v[(myChip8.opcode & 0x0f00) >> 8];
	var vy = myChip8.v[(myChip8.opcode & 0x00f0) >> 4];

	// largest 8 bit unsigned int is 256 so % 256 result of vx+vy
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = (vx + vy) % 256;
	
	// if overflow set v[0xf] to 1 otherwise set to 0
	if((vx + vy) > 255){
		myChip8.v[0xf] = 1;
	} else {
		myChip8.v[0xf] = 0;
	}
	return 1;
}

export function arithmeticSubtract(myChip8){
	var vx = myChip8.v[(myChip8.opcode & 0x0f00) >> 8];
	var vy = myChip8.v[(myChip8.opcode & 0x00f0) >> 4];
	
	var temp = vx - vy;

	// emulate underflow 
	if(temp < 0){
		temp = temp + 255;
	}

	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = temp;
	
	// if overflow set v[0xf] to 1 otherwise set to 0
	if((vx - vy) < 0){
		myChip8.v[0xf] = 0;
	} else {
		myChip8.v[0xf] = 1;
	}
	return 1;
}

export function arithmeticShiftRight(myChip8){
	var vx = myChip8.v[(myChip8.opcode & 0x0f00) >> 8];
	var temp = vx;
	var x = 0
	var leastSigBit = 0;

	// find least significat bit
	for(x = 7; x >= 0; x--){
		if(temp - Math.pow(2, x) >= 0){
			leastSigBit = x;
			temp = temp - Math.pow(2, x);
		}
	}

	// shift right 1 bit
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = vx >> 1;

	// store least sig bit in v[f]
	myChip8.v[0xf] = Math.pow(2, leastSigBit);

	return 1;
}

export function arithmeticSubtractBackwards(myChip8){
	var vx = myChip8.v[(myChip8.opcode & 0x0f00) >> 8];
	var vy = myChip8.v[(myChip8.opcode & 0x00f0) >> 4];
	
	var temp = vy - vx;

	// emulate underflow 
	if(temp < 0){
		temp = temp + 255;
	}

	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = temp;
	
	// if overflow set v[0xf] to 1 otherwise set to 0
	if((vy - vx) < 0){
		myChip8.v[0xf] = 0;
	} else {
		myChip8.v[0xf] = 1;
	}
	return 1;
}

export function arithmeticShiftLeft(myChip8){
	var vx = myChip8.v[(myChip8.opcode & 0x0f00) >> 8];
	var temp = vx;
	var x = 0
	var MostSigBit = 0;

	// find least significat bit
	for(x = 7; x >= 0; x--){
		if(temp - Math.pow(2, x) >= 0){
			MostSigBit = x;
			x = -1;
		}
	}

	// shift left 1 bit
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = vx << 1;

	// store least sig bit in v[f]
	myChip8.v[0xf] = Math.pow(2, MostSigBit);

	return 1;
}

