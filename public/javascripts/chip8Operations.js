
import * as a from './chip8arithmetic.js';

const Chip8Arithmetic = [a.arithmeticSet, a.arithmeticSetOr, a.arithmeticSetAnd, a.arithmeticSetXor, a.arithmeticAdd,
			a.arithmeticSubtract, a.arithmeticShiftRight, a.arithmeticSubtractBackwards, cpuNULL, cpuNULL,
			cpuNULL, cpuNULL, cpuNULL, cpuNULL, a.arithmeticShiftLeft];

/* Erro catching operations, called when opcode that does not corespond to a operation or suboperation is called. Sets
 * pc to 0 and prints error msg to console
 */
export function cpuNULL(myChip8){
	console.log("ERROR instruction ", myChip8.opcode, " at ", (myChip8.pc - 2));

	myChip8.pc = 0;
	return -1;
}

/*
 * OPcode: 00XX
 * XX = SubOperation to execute
 * 0xE0: celar screen
 * 0xEE: return from subroutine
 */

export function cpuZero(myChip8){
	//console.log(myChip8.opcode);
	// if 0x00E0, clear screen
	if(myChip8.opcode == 0x00E0){
		for(var x = 0; x < 100; x++)
			myChip8.screen[x] = new Array();
		myChip8.draw = 1;
		//console.log('Cpu Zero Ox00E0');
		//console.log("Cleared Screen with: ");
		//console.log(myChip8.opcode);
		return 1;
	}

	// if 0x00EE return from subroutine
	if(myChip8.opcode == 0x00EE){
		//console.log("CpuZero 0x00EE");
		//console.log("Stack pointer: ", myChip8.sp);
		//console.log("Stack value: ", myChip8.stack[myChip8.sp -1]);
		myChip8.sp = myChip8.sp - 1;
		//console.log("got pased changing sp");
		myChip8.pc = myChip8.stack[myChip8.sp];
		//console.log("set pc to: ", myChip8.pc);
		return 1;	
	}
	// if get passed two statements above then call cpuNULL
	cpuNULL(myChip8);
	return 0;
}

/*
 * Opcode 1NNN - jump to position NNN by setting PC to NNN
 */
export function cpuJump(myChip8){
	//console.log("cpuJump setting pc to", myChip8.opcode & 0x0fff);	
	myChip8.pc = myChip8.opcode & 0x0fff;
	return 1;
}

/*
 * Opcode 2NNN - store current PC on stack and jump to NNN
 */
export function cpuSubroutine(myChip8){
	// store PC on stack
	myChip8.stack[myChip8.sp] = myChip8.pc;
	// itterate stack pointer by one as we added somehting to the stack
	myChip8.sp = myChip8.sp + 1;
	//console.log("going into subroutine");
	// set pc to NNN
	myChip8.pc = myChip8.opcode & 0x0fff;
	return 1;
}

/*
 * Opcode 3XNN - skips the next instruction if v[x] == NN
 */
export function cpuSkipIfEqual(myChip8){
	//console.log("in cpuSkipIfEqual v[x] = ", myChip8.v[(myChip8.opcode & 0x0f00) >> 8])
	if(myChip8.v[(myChip8.opcode & 0x0f00) >> 8] == (myChip8.opcode & 0x00ff)){
		myChip8.pc = myChip8.pc + 2;
	}
	return 1;
}

/*
 * Opcode 4XNN - skip next instruction if v[x] != NN
 */
export function cpuSkipIfNotEqual(myChip8){
	//console.log("in cpuSkipIfNotEqual");
	if(myChip8.v[(myChip8.opcode & 0x0f00) >> 8] != (myChip8.opcode & 0x00ff)){
		myChip8.pc = myChip8.pc + 2;
	}
	return 1;
}

/*
 * Opcode 4XY0 - skip the next instruction if v[x] == v[y]
 */
export function cpuSkipIfRegistersEqual(myChip8){
	//console.log("cpuSkipIfRegistersEqual");
	if(myChip8.v[(myChip8.opcode & 0x0f00) >> 8] == myChip8.v[(myChip8.opcode & 0x00f0) >> 4]){
		myChip8.pc = myChip8.pc + 2;
	}
	return 1;
}

/* Opcode: 0x6XNN - sets a register to a value
 * X is the register to set
 * NN is the value to set the register to
 */
export function cpuSetRegister(myChip8){
	//console.log("cpuSetRegister");
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = myChip8.opcode & 0x00ff;
	return 1;
}

/*
 * Opcode: 7XNN - Add NN to register X
 */
export function cpuAdd(myChip8){
	//console.log("cpuAdd v["+((myChip8.opcode & 0x0f00)>>8)+"] += "
//		+(myChip8.opcode & 0x00ff));
	//console.log(`${myChip8.v[(myChip8.opcode & 0x0f00) >> 8]} += ${(myChip8.opcode & 0x00ff)}`);
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8] = 
		(myChip8.v[(myChip8.opcode & 0x0f00) >> 8] + (myChip8.opcode & 0x00ff)) % 256;
	return 1;
}

/*
 * Opcode: 8XYK - Use arithmetic operation.
 * X - register 1
 * Y - register 2
 * K operation to use
 */
export function cpuArithmetic(myChip8){
	var temp = myChip8.opcode & 0x000f;
	//console.log("Arithmetic function #: ", temp);
	return Chip8Arithmetic[temp](myChip8);
}

/*
 * Opcode: 9XY0 - skips next instruction if v[x] != v[y]
 */
export function cpuSkipIfRegisterNotEqual(myChip8){
	//console.log("cpuSkipIfRegisterNotEqual");
	if(myChip8.v[(myChip8.opcode & 0x0f00) >> 8] != myChip8.v[(myChip8.opcode & 0x00f0) >> 4]){
		myChip8.pc = myChip8.pc + 2;
	}
	return 1;
}

/*
 * OPcode 0xANNN - set register i to NNN
 */
export function cpuSetIRegister(myChip8){
	//console.log("cpuSetIRegister");
	myChip8.i = myChip8.opcode & 0x0fff;
	return 1;
}

/*
 * Opcode BNNN - set PC to NNN + v[0]
 */

export function cpuJumpPlusRegister(myChip8){
	//console.log("cpuJumpPlusReigster");
	myChip8.pc = (myChip8.opcode & 0x0fff) + myChip8.v[0];
	return 1;
}

/*
 * Opcode CXNN - sets v[x] to a random number & NN
 */
export function cpuRand(myChip8){
	//console.log("cpuRand");
	myChip8.v[(myChip8.opcode & 0x0f00) >> 8 ] = 
		(Math.floor(Math.random() * 256) % 0xff) & (myChip8.opcode & 0x00ff);
	return 1;
}

/*
 * Opcode 0xDXYN - Draw sprite from the memory location stored in register I. If a pixel is changed set v[0xf] to 1. 
 * XOR drawing. start sprites at position v[X] V[Y]. N is the number of 8 bit rows that are to be drawn, adding 1 to 
 * V[y] every row.
 */
export function cpuDrawSprite(myChip8){
	//console.log("cpuDrawSprite");
	var x = (myChip8.opcode & 0x0f00) >> 8;
	var y = (myChip8.opcode & 0x00f0) >> 4;
	var n = myChip8.opcode & 0x000f;
	var count = 0; // keeps track of characters printed on a line
	var lineCount = 0; // how many lines have been printed
	var i;
	var pixel;
	
	myChip8.v[0xf] = 0;

	x = myChip8.v[x];
	y = myChip8.v[y];

	//console.log(`x: ${x}, y: ${y}, n: ${n}`);
	
	for(lineCount = 0; lineCount < n; lineCount++){
		pixel = myChip8.mem[myChip8.i + lineCount];
		for(count = 0; count < 8; count++){
			if((pixel & (0x80 >> count)) != 0){
				if(myChip8.screen[(x + count)%64][(y + lineCount)%32] ==1)
					myChip8.v[0xf] = 1;
				myChip8.screen[(x + count)%64][(y + lineCount)%32] ^= 1;
			}
		}
	}
	myChip8.draw = 1;
	/*
	while((n-lineCount) != 0){
		i = myChip8.mem[myChip8.i + lineCount];
		for(count = 0; count < 8; count++){
			if(i - Math.pow(2, 7 - count) >=0){
				i = i - Math.pow(2, 7 - count);
				if(myChip8.screen[(x + count) % 64][(y + lineCount) % 32] == 1){
					myChip8.v[0xf] = 1;
					myChip8.screen[(x + count) % 64][(y + lineCount) % 32] = 0;
					// I think I need to set draw here to 1 but that's not how orginal
					// implementation worked
				} else {
					myChip8.screen[(x + count) % 64][(y + lineCount) % 32] = 1;
					myChip8.draw = 1;
				}
			}
		}
		lineCount = lineCount + 1;
	}*/
	return 1;
}

/*
 * Opcode EXKK -
 * When KK = 0xA1 - if input[v[x]] is not currently pressed skip next instruction
 * When KK = 0x9E - if input[v[x]] is pressed skip nexty instruction
 * always set input[v[x]] to zero if it's not already
 */
export function cpuKeyInput(myChip8){
	//console.log("cpuKeyINput");
	var x = myChip8.v[(myChip8.opcode & 0x0f00) >> 8];
	var k = myChip8.opcode & 0x00ff;

	if(k == 0xa1){
		if(myChip8.input[x] != 1 ){
			myChip8.pc = myChip8.pc + 2;	
		}
		myChip8.input[x] = 0;
		return 1;
	}
	if(k == 0x9e){
		if(myChip8.input[x] == 1){
			myChip8.pc = myChip8.pc + 2;
			myChip8.input[x] = 0;
		}
		return 1;
	}
	cpuNULL(myChip8);
	return 0;
}

/*
 * opcode FXKK with KK being the operation to perform
 */
export function cpuF(myChip8){
	//console.log("in cpuF");
	var x = (myChip8.opcode & 0x0f00) >> 8;
	var k = myChip8.opcode & 0x00ff;
	
	if(k == 0x07){
	//	console.log("setting v[x] to: ", myChip8.delay)
		myChip8.v[x] = myChip8.delay;	
		return 1;
	}
	if(k == 0x0A){
		// this should wait on next keyboard input, right now it's just taking the 
		// last key hit before this instruction is called
	//	console.log("got keyboard input", myChip8.keyboardInput);
	//	console.log("storing input in in v[",x,"]");
		myChip8.v[x] = myChip8.keyboardInput; 
		return 1;
	}
	if(k == 0x15){
		myChip8.delay = myChip8.v[x];
		return 1;	
	}
	if(k == 0x18){
		myChip8.sound = myChip8.v[x];
		return 1;
	}
	if(k == 0x1E){
		myChip8.i = myChip8.i + myChip8.v[x];
		return 1;
	}
	if(k == 0x29){
		if(myChip8.v[x] * 5 > 80){
	//		console("error not a character 0-f");
			return 0;
		}
		myChip8.i = myChip8.v[x] * 5;
		return 1;
	}
	if(k == 0x33){
		myChip8.mem[myChip8.i] = parseInt(myChip8.v[x] / 100);
		myChip8.mem[myChip8.i + 1] = parseInt(myChip8.v[x] / 10) % 10;
		myChip8.mem[myChip8.i + 2] = parseInt(myChip8.v[x] % 100) % 10;
		return 1;
	}
	if(k == 0x55){
		for(k = 0; k <= x; k++){
			myChip8.v[myChip8.i + k] = myChip8.v[k];
		}
		return 1;
	}
	if(k == 0x65){
		for(k = 0; k <= x; k++){
			myChip8.v[k] = myChip8.mem[myChip8.i + k];
		}
		return 1;
	}

	// if none of the above were executed then instruction not implemented.
	cpuNULL(myChip8);
	return 0
}


















