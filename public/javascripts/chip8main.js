/*
 * Author: Derek Davie
 * Last Updated: 11-16-20
 * CHIP-8 emulator port of TBCE found here: https://github.com/DerekDavie/TBCE which was also built by me with the help
 * of: http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/
 */

// import chip8 class
import {chip8} from './chip8class.js';

window.onload = function() {
	document.getElementById('chip8Button').onclick = startChip;
}
function startChip(){
	
	var myChip8 = new chip8();

	// add listener for keypress, have the retrieveInput method from the chip8 class process and store
	// any keypree in myChip8
	document.addEventListener('keypress', function(event){
		myChip8.retrieveInput(event, myChip8);
	});

	var screen = initScreen(64*8, 32*8);

	myChip8.loadProgram('./ROMS/PONG');
	runCycle(myChip8, screen)	
}

function runCycle(myChip8, screen) {	
	//console.log("in main about to execute: ", myChip8.pc);	
	myChip8.executeCycle();

	if(myChip8.draw != 0){
		draw(myChip8, screen, 8);
		myChip8.draw = 0;
	}

	// slow down cycle to 60hz somehow

	if(myChip8.pc == 0)
		return 1;
	setTimeout(runCycle, 5, myChip8, screen);
}


// draw whole screen based on myChip8.screen
function draw(myChip8, screen, scale){

	var x;
	var y;
	var height = 32 * scale;
	var width = 64 * scale;

	for(x = 0; x < width; x += scale){
		for(y = 0; y < height; y += scale){
			if(myChip8.screen[x/scale][y/scale] == 1){
				screen.fillStyle = "#FFFFFF";
				screen.fillRect(x, y, scale, scale);
			} else {
				screen.fillStyle = "#000000";
				screen.fillRect(x, y, scale, scale);
			}
		}
	}
	return 1;
}

// return screen object while also resizing and clearing the canvas
function initScreen(width, height){
	var canvas = document.getElementById('chip8Screen');

	// resize canvas to passed values
	canvas.width = width;
	canvas.height = height;

	// pull out 2d screen we can use to draw on
	var screen = canvas.getContext("2d");
	
	// color entire screen red
	screen.fillStyle = "#FF0000";
	screen.fillRect(0,0,width,height);
	
	return screen;
}


