const express = require('express');
const app = express();
const path = require('path');
const moment = require('moment');


const PORT = process.env.PORT || 3000;


// simple loging middleware
const logger = (req, res, next) => {
	console.log(`${req.protocol}://${req.get('host')}${req.originalUrl} : ${moment().format()}`);
	next();
}

// Init middleware
app.use(logger);

// Body parser middleware 
app.use(express.json());
app.use(express.urlencoded({ extended : false}));


app.listen(PORT, function(){
	console.log(`Server started on port ${PORT}`);
});

// Members API routes
app.use('/api/members', require('./routes/api/members'));

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public/javascripts')));

app.get('/', function(req, res) {
	console.log("Hello World");
	res.sendFile(path.join(__dirname, 'public', 'index.html'));
	res.end();
});

